var express = require('express')
var bodyParser = require('body-parser');
var app = express();

var mongojs = require('mongojs')

var db = mongojs('mongodb://127.0.0.1/palestra', ['usuarios'])

var ObjectId = mongojs.ObjectId;

app.use('/static', express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/usuario/:id*?', function(req, res) {
    var filtro = {};
    if (req.params.id) {
        filtro['_id'] = ObjectId(req.params.id)
    }
    db.usuarios.find(filtro, function(erro, usuarios) {
        if (erro) {
            console.log(erro)
            res.status(500).send(erro)
        } else {
            res.json(usuarios)
        }
    })
});

app.put('/usuario/:id*?', function(req, res) {
    var usuario = req.body;
    var id = ObjectId(req.params.id);

    db.usuarios.update({'_id': id}, usuario, {upsert:true}, function(erro, usuarioSalvo) {
        if (erro) {
            console.log(erro)
            res.status(500).send(erro)
        } else {
            res.json(usuarioSalvo)
        }
    });
});

app.delete('/usuario/:id', function(req, res) {
    var ObjectId = mongojs.ObjectId;
    db.usuarios.remove({'_id': ObjectId(req.params.id)}, function(erro, removidos) {
        if (erro) {
            console.log(erro)
            res.status(500).send(erro)
        } else {
            res.json(removidos)
        }
    })
});

app.listen(3003, function() {
    console.log('Express server online!')
});


