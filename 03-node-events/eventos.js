var events = require('events');

var emitter1 = new events.EventEmitter();

emitter1.on('nomeEvento', function(arg) {
    console.log('Listener Evento: ' + arg);
});

emitter1.emit('nomeEvento', 1);
