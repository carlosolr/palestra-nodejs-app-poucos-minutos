module.exports = (function() {
    var fs = require('fs');

    var lerArquivo = function(callback) {
        fs.readFile('usuarios.json', 'utf8', function(erro, dados) {
            if (erro)  {
                console.log(erro);
                callback(null);
            } else {
                callback(JSON.parse(dados));
            }
        });
    };

    var escreverArquivo = function(dados, callback) {
        fs.writeFile('usuarios.json', JSON.stringify(dados), 'utf8', callback);
    };

    return {
        lerArquivo: lerArquivo,
        escreverArquivo: escreverArquivo
    }

})();