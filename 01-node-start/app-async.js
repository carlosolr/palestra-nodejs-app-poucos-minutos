console.log('[notTimeout 1] - ' + new Date())

setTimeout(function() {
    console.log('[setTimeout 1] - ' + new Date())
}, 2000)

setTimeout(function() {
    console.log('[setTimeout 2] - ' + new Date())
}, 1000)

console.log('[notTimeout 2] - ' + new Date())

