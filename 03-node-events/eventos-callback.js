var events = require('events');

var emitter = new events.EventEmitter();

emitter.on('somar20', function(arg, callback) {
    callback(arg + 20);
});

emitter.emit('somar20', 10, function(resultado) {
    console.log(resultado)
});