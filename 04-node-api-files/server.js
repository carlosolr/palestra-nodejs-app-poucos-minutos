var express = require('express')
var bodyParser = require('body-parser');
var app = express();

var gerenciadorArquivo = require('./gerenciadorArquivo')

app.use('/static', express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/usuario', function(req, res) {
    gerenciadorArquivo.lerArquivo(function(usuarios) {
        if (usuarios) {
            res.json(usuarios)
        } else {
            res.send('No Data!')
        }
    })
});

app.put('/usuario', function(req, res) {
    gerenciadorArquivo.lerArquivo(function(usuarios) {
        var usuario = req.body;
        if (!usuarios) {
            usuarios = {}
        }
        usuarios[usuario.id] = usuario
        gerenciadorArquivo.escreverArquivo(usuarios, function(erro) {
            if (erro) {
                res.status(500).send(erro)
            } else {
                res.send('salvo')
            }
        });
    })
});

app.get('/usuario/:id', function(req, res) {
    gerenciadorArquivo.lerArquivo(function(usuarios) {
        if (usuarios) {
            var usuario = usuarios[req.params.id];
            if (usuario) {
                res.json(usuario);
            } else {
                res.send('Not Found!')
            }
        } else {
            res.send('Not Found!')
        }
    })
});

app.delete('/usuario/:id', function(req, res) {
    gerenciadorArquivo.lerArquivo(function(usuarios) {
        if (usuarios) {
            var usuario = usuarios[req.params.id];
            if (usuario) {
                usuarios[req.params.id] = null;
                res.send('Removido usuário: ' + JSON.stringify(usuario))
            } else {
                res.send('Not Found!')
            }
        } else {
            res.send('Not Found!')
        }
    })
});

app.listen(3003, function() {
    console.log('Express server online!')
});


