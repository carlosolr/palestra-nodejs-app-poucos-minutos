#Como criar uma aplicação NodeJs em poucos minutos

Palestra criada inicialmente para o evento 2º Solutions Day Vale do paraíba

<br>

#### Exemplos apresentados

| Diretório | Descrição |
| ------ | ------ |
| 01-node-start | Código simples para mostrar a execução de scripts e funções básicas |
| 02-node-simple-server | Criação de um node server baseado no código nativo |
| 03-node-events | Funcionamento da estrutura de EventEmitter |
| 04-node-api-files | Aplicação simples com <i>express</i> de uma API <b>GET</b>/<b>PUT</b>/<b>DELETE</b> que utiliza arquivo para persistir dados |
| 05-node-api-mongodb | Aplicação simples com <i>express</i> e <i>mongodb</i> para persistir dados de uma API <b>GET</b>/<b>PUT</b>/<b>DELETE</b> |

<br>

#### Inicialização

Rodar ```npm install``` para os exemplos <i>04-node-api-files</i> e <i>05-node-api-mongodb</i>
