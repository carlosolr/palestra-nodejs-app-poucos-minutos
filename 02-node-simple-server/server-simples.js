var http = require('http')

http.createServer(function(req, res) {
    switch (req.url) {
        case '/':
            res.end('<h1>Hello World!!!</h1>');
            break;
        case '/folks':
            res.end('<h1>Hello folks!</h1>');
            break;
        default:
            res.statusCode = 404
            res.end()
    }
}).listen(3002);

